# **WEEK 1**
## Principles & Practices

In Pre-Fabacademy 2020 we started doing a website as there are many different websites to create a personal site, but we preferred to use Gitlab website to document all what we do weekly. But before that, we have to think about something to make, something new, useful and can make difference in the life.

However, to create a website, as I mentioned that we have to use a Gitlab website to create a personal website called "Jawaher Website" for the weekly documentations, downloading Git Bash, and slack for the communication and

<code>etc.</code>

And now, we will go through the steps :

1. Go to [GitLab Website](gitlab.com) and register with a new account (should be your name)

![](Gitlab-logo.jpg)

2. Download [Git Bash](https://www.atlassian.com/git/tutorials/git-bash) Application for windows or Mac

![](Gitbash-logo.png)

3. Go to [Slack website](slack.com) and sign up

![](Slack-logo.png)


4. Download Slack App on your iPhone and join Fabacademy2020 group to contact Mr. Hesham and the students easily.



5. Start configuring the Git Bash with the username and email.


### _GitLab_ :
  _Now, after I logged in, you need to create a new project:_

  - Press **NEW Project**

  ![](new-project.PNG)

  - Name the project and write the description, follow the below picture:

  ![](project-name.PNG)

  - After creating the project, Open **Git Bash** program

### _Git Bash_:
  _Then we did the configuration using the commands:_

  <code>git config --global user.name "Jawaher Qasem"</code>

  <code>git config --global user.email "Jawaher.Qasem@gmail.com"</code>

  _To recognize the username in Gitlab:_

  <code>ssh-keygen -t rsa -b 4096 -c "jawaher.qasem@gmail.com"</code>

  ![](git-config.png)

  _To make sure what is the user of the computer:_ <code>whoami</code>

  _To start the path:_ <code>cd</code>

  _to check the list in that path:_ <code>ls</code>


  _To make a new folder in the desktop, first you have to open desktop path, then make new folder, then if you want to add text file inside the new folder, follow the below picture:_

  ![](make-newfolder.png)
